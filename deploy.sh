chmod 400 keypair.pem

ssh -i "keypair.pem" ubuntu@ec2-3-139-79-69.us-east-2.compute.amazonaws.com

cd /home/ubuntu/palindrome

git reset --hard

git pull

docker stop $(docker ls –aq)

docker rm $(docker ls –aq)

docker rmi $(docker images -a -q)

docker build -t palindrome .

docker push docfiles/palindrome:latest

docker run -itd palindrome:latest

